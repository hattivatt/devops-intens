FROM node

RUN mkdir /flatris
COPY package.json /flatris
WORKDIR /flatris
RUN yarn install

COPY . /flatris
RUN yarn test 
RUN yarn build

EXPOSE 3000

CMD yarn start
